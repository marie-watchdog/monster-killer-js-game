const ATTACK_VALUE = 10
const STRONG_ATTACK_VALUE = 17
const MONSTER_ATTACK_VALUE = 14
const HEAL_VALUE = 20

const MODE_ATTACK = 'ATTACK'
const MODE_STRONG_ATTACK = 'STRONG_ATTACK'

const LOG_EVENT_MONSTER_ATTACK = 'MONSTER_ATTACK'
const LOG_EVENT_PLAYER_ATTACK = 'PLAYER_ATTACK'
const LOG_EVENT_PLAYER_STRONG_ATTACK = 'PLAYER_STRONG_ATTACK'
const LOG_EVENT_PLAYER_HEAL = 'PLAYER_HEAL'
const LOG_EVENT_GAME_OVER = 'GAME_OVER'

let chosenMaxLife
let hasBonusLife = true
let battleLog = []

function getUserInput() {
    const enteredValue = prompt(
        'Introduce health value for you and the monster',
        '100'
    )
    let parsedValue = parseInt(enteredValue)

    if (isNaN(parsedValue) || parsedValue <= 0) {
        throw { massage: 'Invalid input, not a number! Health set to 100.' }
    }
    return parsedValue
}
try {
    chosenMaxLife = getUserInput()
} catch (error) {
    console.log(error)
    chosenMaxLife = 100
}

let currentPlayerHealth = chosenMaxLife
let currentMonsterHealth = chosenMaxLife
adjustHealthBars(chosenMaxLife)

function endRound() {
    const initialPlayerHealth = currentPlayerHealth
    const playerDamage = dealPlayerDamage(MONSTER_ATTACK_VALUE)
    currentPlayerHealth -= playerDamage

    writeToLog(
        LOG_EVENT_MONSTER_ATTACK,
        playerDamage,
        currentMonsterHealth,
        currentPlayerHealth
    )

    if (currentPlayerHealth <= 0 && hasBonusLife) {
        currentPlayerHealth = initialPlayerHealth
        hasBonusLife = false
        alert('You were saved by bonus life!')
        removeBonusLife()
        setPlayerHealth(initialPlayerHealth)
    }
    if (currentMonsterHealth <= 0 && currentPlayerHealth > 0) {
        alert('You won!')
        writeToLog(
            LOG_EVENT_GAME_OVER,
            'PLAYER WON',
            currentMonsterHealth,
            currentPlayerHealth
        )
    } else if (currentPlayerHealth <= 0 && currentMonsterHealth > 0) {
        alert('You lost!')
        writeToLog(
            LOG_EVENT_GAME_OVER,
            'MONSTER WON',
            currentMonsterHealth,
            currentPlayerHealth
        )
    } else if (currentMonsterHealth <= 0 && currentPlayerHealth <= 0) {
        alert('You have a draw!')
        writeToLog(
            LOG_EVENT_GAME_OVER,
            'A DRAW',
            currentMonsterHealth,
            currentPlayerHealth
        )
    }
    if (currentMonsterHealth <= 0 || currentPlayerHealth <= 0) {
        reset()
    }
}

function reset() {
    currentPlayerHealth = chosenMaxLife
    currentMonsterHealth = chosenMaxLife
    resetGame(chosenMaxLife)
    if (hasBonusLife === false) {
        restoreBonusLife()
        hasBonusLife = true
    }
}

function attackMonster(attackMode) {
    const maxDamage =
        attackMode === MODE_ATTACK ? ATTACK_VALUE : STRONG_ATTACK_VALUE
    const logEvent =
        attackMode === MODE_STRONG_ATTACK
            ? LOG_EVENT_PLAYER_STRONG_ATTACK
            : LOG_EVENT_PLAYER_ATTACK
    const damage = dealMonsterDamage(maxDamage)
    currentMonsterHealth -= damage
    writeToLog(logEvent, damage, currentMonsterHealth, currentPlayerHealth)
    endRound()
}

function writeToLog(ev, value, monsterHealth, playerHealth) {
    let logEntry = {
        event: ev,
        value: value,
        finalMonsterHealth: monsterHealth,
        finalPlayerHealth: playerHealth,
    }
    battleLog.push(logEntry)
}

function healPlayerHandler() {
    let healValue
    if (currentPlayerHealth >= chosenMaxLife - HEAL_VALUE) {
        alert('You cant heal to more than your initial health!')
        healValue = chosenMaxLife - currentPlayerHealth
    } else {
        healValue = HEAL_VALUE
    }
    increasePlayerHealth(healValue)
    currentPlayerHealth += healValue
    writeToLog(
        LOG_EVENT_PLAYER_HEAL,
        healValue,
        currentMonsterHealth,
        currentPlayerHealth
    )
    endRound()
}

function attackHandler() {
    attackMonster(MODE_ATTACK)
}

function strongAttackHandler() {
    attackMonster(MODE_STRONG_ATTACK)
}

function logHandler() {
    for (let i = 0; i < battleLog.length; i++) {
        console.log('---', battleLog[i], '---')
    }
    console.log('--- End of the entries ---')
}

attackBtn.addEventListener('click', attackHandler)
strongAttackBtn.addEventListener('click', strongAttackHandler)
healBtn.addEventListener('click', healPlayerHandler)
logBtn.addEventListener('click', logHandler)
