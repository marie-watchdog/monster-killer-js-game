## Simple JS "Monster Killer" game.

The purpose of this project is to practice with JS control structures (i.g. if's, try-catch, functions).

### The game works the following way:

Once you open _index.html_ you'll be asked to input a value representing amount of health for both you and a monster (default value is 100). 

Then you can launch an **attack** or a **strong attack** (the exact values can be changed in the code).

If you lost some health you can **heal** (the value also can be changed in the code).

You also can check a **game log** with all the moves (Note that it can be seen in the DevTools console).

You as a player also have **one extra life** (indicator with number 1), so in case your health drops below zero, extra life is used, health is restored back to the previous (before the last attack) amount and you can continue playing.